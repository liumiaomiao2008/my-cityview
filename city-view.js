const accessKey = 'N5jQmU3glgUtS6-Xx8ki33fPxC_7UAXKdojTeB-gTPE'
const baseURL = 'https://api.unsplash.com/'
const eleInput = document.querySelector('.inputCity')
const eleSearch = document.querySelector('.fa-search')
const eleArrowLeft = document.querySelector('.fa-arrow-alt-circle-left')
const eleArrowRight = document.querySelector('.fa-arrow-alt-circle-right')
let eleSingle = document.querySelector('.showSingle')
let eleDes = document.querySelector('.caption')
let pageCount = 1
let inputResult = 'Toronto'
let myIndex = 0
let eleImgArr = []
let inputAllResult = []
let descriptionImg = 'eleDes'
let index = 'imgIndex'
let userInput = 'userInput'
let userInputAll = 'userInputAll'
let regularURL = 'regularURL'
let currentPageNumber = 'current-page-number'
let currentIndex = 'current-index'
let totalPage

//let showMulti = document.querySelector('.showMulti')
let count = 0



const clickImg = evt => {
    myIndex = evt.target.getAttribute(index)
    localStorage.setItem(currentIndex, myIndex)
    //console.log(myIndex)
    //console.log(document.getElementsByTagName('img')[myIndex].src)
    let imgURL = document.getElementsByTagName('img')[myIndex].getAttribute(regularURL)
    eleSingle.style.backgroundImage = `url('${imgURL}')`
    eleDes.innerHTML = document.getElementsByTagName('img')[myIndex].getAttribute(descriptionImg)
}

const searchFun = () => {
    //console.log(eleInput.value.trim())
    eleInput.select()//select all texts: all selected effect:
    inputResult = eleInput.value.trim().toLowerCase()
    inputAllResult.push(inputResult)

    //console.log('key word===>',inputResult)
    //anything window.  ===> global variable
    //window.localStorage.setItem('userInput', inputResult) === localStorage.setItem('userInput', inputResult)

    localStorage.setItem(userInput, JSON.stringify(inputResult))
    localStorage.setItem(userInputAll, JSON.stringify(inputAllResult))
    localStorage.setItem(currentPageNumber, 1)
    localStorage.setItem(currentIndex, 0)

    fetchCity(0)
}

const inputSearch = evt => {
    //evt.target.select()
    evt.key === 'Enter' && searchFun()
}

const showNext = () => {
    pageCount++
    if (pageCount > totalPage){
        pageCount = 1
    }
    localStorage.setItem(currentPageNumber, pageCount)
    //pageCount = JSON.parse((localStorage.getItem(currentPageNumber)))
    localStorage.setItem(currentIndex, 0)
    fetchCity(0)

}
const showPrevious = () => {
    pageCount--
    if (pageCount < 1){
        pageCount += totalPage
    }
    localStorage.setItem(currentPageNumber, pageCount)
   // pageCount = JSON.parse((localStorage.getItem(currentPageNumber)))
    localStorage.setItem(currentIndex, 0)
    fetchCity(0)
}

const fetchCity = (c) => {
    inputResult = JSON.parse(localStorage.getItem(userInput))
    pageCount = JSON.parse((localStorage.getItem(currentPageNumber)))
    console.log(inputResult)
    // if (inputResult === null) {
    //     inputResult = 'toronto'
    // }
    // if (!inputResult) {
    //     inputResult = 'toronto'
    // }
    //null meaning false
    !inputResult && (inputResult = 'toronto')
    fetch(`${baseURL}search/photos?page=${pageCount}&client_id=${accessKey}&query=${inputResult}&orientation=landscape`)
        .then(response => response.json())
        //.then(data => console.log(data))
        .then(data => {
            //console.log(data.results.length)
            if(data.results.length !== 0){
                document.querySelector('.show').style.display = 'flex'
                console.log('data===>', data)
                totalPage = data.total_pages
                //console.log(totalPage)
                //console.log(data.results[0].urls.regular)
                let result = data.results[c].urls.regular
                let des = data.results[c].alt_description
                eleSingle.style.backgroundImage = `url('${result}')`
                eleDes.innerHTML = des
                for (let i = 0; i < 10; i++) {
                    let eleImg = document.getElementsByTagName('img')
                    let eleDes = data.results[i].alt_description
                    eleImg[i].setAttribute(regularURL,data.results[i].urls.regular)
                    eleImg[i].setAttribute(descriptionImg, eleDes)
                    eleImg[i].setAttribute(index, i)
                    eleImg[i].src = data.results[i].urls.small
                    eleImg[i].addEventListener('click', clickImg)
                    eleImg[i].className = 'imgItem'
                    eleImgArr.push(eleImg[i])
                }
            }else if (data.results.length === 0){
                eleSingle.style.backgroundImage = `url('https://blog.thomasnet.com/hs-fs/hubfs/shutterstock_774749455.jpg?width=1200&name=shutterstock_774749455.jpg')`
                document.querySelector('.show').style.display = 'none'
                eleDes.innerHTML = '404 error'
            }
            console.log(pageCount)

            }
        )
}

eleSearch.addEventListener('click', searchFun)
eleInput.addEventListener('keydown', inputSearch)
//all selected effect: when focus, all the content will be selected
eleInput.addEventListener('focusin', () => {
    eleInput.select()
})
eleArrowRight.addEventListener('click', showNext)
eleArrowLeft.addEventListener('click', showPrevious)

//console.log(localStorage.getItem(currentIndex))
count = JSON.parse(localStorage.getItem(currentIndex))
//console.log(count)
!count && fetchCity(0) || fetchCity(count)
count = 0

//count === null && console.log(3333)