//google image
//all image/data are downloaded from server to your local laptop

//through protocols: such as HTTP
//HTTP is a protocol for fetching resources such as HTML Document
//https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview

//how? API -> Application Program Interface
//1.XMLHttp request API (core of Ajax: asynchronous JS and XML <text with style>)
//2. Fetch API(2015~): returns a promise (response to the request)
//3. Axios: promise based HTTP client, which uses XMLHttpRequest Internally

fetch('https://jsonplaceholder.typicode.com/users')
.then(response => response.json())//json() is a method which is to process 'response'
    //the json() method of the response interface takes a Response stream and reads it to completion
    //it returns a promise which resolves with the result of parsing the body text as JSON
.then(data => console.log(data))
.catch(err => console.log(err))
.finally(()=>console.log('game over'))

console.log('the line is running')

//async, sync
//synchronous:
//|--A--|
//      |--B--|
//asynchronous: high efficiency
//|--A--|
//   |--B--|

//Promise
//A promise has the following states:
//1. pending: initial state, neither fulfilled nor rejected
//2.fulfilled: meaning that the operation was completed
//3.rejected:meaning that the operation failed
//4.settled

